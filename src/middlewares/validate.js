const {validationResult} = require('express-validator');

module.exports = (req, res, next) => {
    const result = validationResult(req);
    if (!result.isEmpty()) {
        let arr = [];
        result.array().map((err) => arr.push(`${err.param} ${err.msg}`));
        return res.status(422).json({status:'Validation Error',errorMessages:arr});
    }

    next();
};