const {Strategy, ExtractJwt} = require('passport-jwt');
let secretKey = require("../helpers/config.secret");

const User = require('../models/user');

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secretKey
};
console.log(secretKey)
module.exports = passport => {
    passport.use(
        new Strategy(opts, (jwt_payload, done) => {
            console.log("jwt_payload ",jwt_payload);
            User.findByPk(jwt_payload.userId)
                .then(user => {
                    if (user) return done(null, user);
                    return done(null, false);
                })
                .catch(err => {
                    return done(err, false, {message: 'Server Error'});
                });
        })
    );
};
