const sequelize = require("../helpers/db/init");
const { DataTypes } = require("sequelize");
const Category=require("./category");
const Movie = sequelize.define("Movie", {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    rate: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    image: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    categoryId: {
        type: DataTypes.INTEGER,
        allowNull: true
    }
});

Movie.belongsTo(Category,{
    foreignKey:"categoryId"
});
Category.hasMany(Movie,{
    foreignKey:"categoryId"
});

module.exports = Movie;
