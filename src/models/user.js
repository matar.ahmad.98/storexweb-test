const bcrypt = require("bcryptjs");
const sequelize = require("../helpers/db/init");
const { DataTypes } = require("sequelize");
const User = sequelize.define("User", {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique:true
    },
    BDate: {
        type: DataTypes.DATE,
        allowNull: true,
        set(value) {
            this.setDataValue('BDate',new Date(value).toISOString())
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
        set(value) {
            this.setDataValue('password', bcrypt.hashSync(value, 8));
        }
    }
});


module.exports = User;
