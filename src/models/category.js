const sequelize = require("../helpers/db/init");
const { DataTypes } = require("sequelize");
const Category = sequelize.define("Category", {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    }
});


module.exports = Category;
