let router = require('express').Router();
let userController = require('../controllers/user');
const validate = require("../middlewares/validate");
const {APIHandler} = require("../helpers/APIHandler");
const {signInValidators, createValidators} = require("../helpers/Validator");
const auth = require("../middlewares/authenticate");



/* GET users listing. */
router.post('/login', signInValidators, validate, APIHandler(userController.signIn));
router.post('/create', createValidators, validate, APIHandler(userController.create));
router.get('/',auth,  APIHandler(userController.getAll));

module.exports = router;
