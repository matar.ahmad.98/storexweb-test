const router = require('express').Router();
const auth = require("../middlewares/authenticate");


router.use('/user', require('./user'));
router.use('/movie',auth, require('./movie'));
router.use('/category',auth, require('./category'));

module.exports = router;
