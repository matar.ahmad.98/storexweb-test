let router = require('express').Router();
let movieController = require('../controllers/movie');
const {APIHandler} = require("../helpers/APIHandler");
const upload = require("../helpers/uploadSingleImage");

router.get('/', APIHandler(movieController.getAll));
router.post('/',upload.single('image'), APIHandler(movieController.create));
router.put('/:id', APIHandler(movieController.update));
router.delete('/:id', APIHandler(movieController.delete));
module.exports = router;
