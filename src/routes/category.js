let router = require('express').Router();
let categoryController = require('../controllers/category');
const {APIHandler} = require("../helpers/APIHandler");
const {createOrUpdateCategoryValidators} = require("../helpers/Validator");
const validate = require("../middlewares/validate");

router.get('/', APIHandler(categoryController.getAll));
router.post('/',createOrUpdateCategoryValidators,validate, APIHandler(categoryController.create));
router.put('/:id',createOrUpdateCategoryValidators,validate, APIHandler(categoryController.update));
router.delete('/:id', APIHandler(categoryController.delete));
module.exports = router;
