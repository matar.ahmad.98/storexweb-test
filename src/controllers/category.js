let categoryModel = require('../models/category');
let {responseSender} = require('../helpers/APIResponse');


exports.getAll = async (req, res) => {
    let category = await categoryModel.findAll();
    return responseSender(res, category);
};
exports.create = async (req, res) => {
    let category = await categoryModel.create({title: req.body.title});
    return responseSender(res, category)
};
exports.update = async (req, res) => {
    await categoryModel.update({title:req.body.title},{where:{id:req.params.id}});
    return responseSender(res);
};
exports.delete = async (req, res) => {
    await categoryModel.destroy({where: {id:req.params.id}});
    return responseSender(res);
};