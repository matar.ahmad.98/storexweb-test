const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
let userModel = require('../models/user');
let {responseSender, responseErrorSender} = require('../helpers/APIResponse');
let secretKey = require("../helpers/config.secret");

exports.signIn = async (req, res) => {
    console.log(secretKey);
    let {email, password} = req.body;
    let user = await userModel.findOne({where:{email}});
    if (!user)
        return responseErrorSender(res, "there is no user with this email", null, 401);

    let passwordIsValid = bcrypt.compareSync(password, user.password);

    if (!passwordIsValid) {
        return responseErrorSender(res, "wrong password", 401);
    }

    const token = jwt.sign({userId: user.id}, secretKey, {
        expiresIn: 86400 * 720 // 2 years
    });
    return responseSender(res, {token, user})
};

exports.create = async (req,res) => {
    try {
        let user = await userModel.create(req.body);
        responseSender(res,user)
    }catch (e) {
        console.log(e)
        throw e;
        responseErrorSender(res,e.message)
    }
};

exports.getAll = async (req,res) => {
    try {
        let user = await userModel.findAll(req.body);
        responseSender(res,user)
    }catch (e) {
        console.log(e)
        throw e;
        responseErrorSender(res,e.message)
    }
};