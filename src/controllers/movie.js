let movieModel = require('../models/movie');
let categoryModel = require('../models/category');
let {responseSender} = require('../helpers/APIResponse');
const {Op} = require("sequelize");


exports.getAll = async (req, res) => {
    let filterArr = [];
    for (const key in req.query) {
        if (["title", "categoryId", "rate"].indexOf(key) > -1) {
            let obj = {};
            obj[key] = req.query[key];
            filterArr.push(obj)
        }
    }
    let movie = await movieModel.findAll({include: categoryModel, where: {[Op.and]:filterArr}});
    return responseSender(res, movie);
};
exports.create = async (req, res) => {
    let movie = await movieModel.create({...req.body, image: req.file.filename});
    return responseSender(res, movie)
};
exports.update = async (req, res) => {
    await movieModel.update({title: req.body.title, description: req.body.description}, {where: {id: req.params.id}});
    return responseSender(res);
};
exports.delete = async (req, res) => {
    await movieModel.destroy({where: {id: req.params.id}});
    return responseSender(res);
};