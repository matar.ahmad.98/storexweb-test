let multer = require('multer');
const path = require('path');
let storage = multer.diskStorage({

  destination: async function (req, file, cb) {
    await cb(null, path.join(__dirname, '../../src/public'))
  },
  filename: async function (req, file, cb) {
    await cb(null, "new_" + Date.now() + path.extname(file.originalname))
    // cb(null, file.originalname)
  }
});
module.exports = multer({ storage: storage, dest: path.join(__dirname, '../../src/public') });