exports.responseSender = (res, result, status, code) => {
    return res.status(code || 200).json({
        status: status || "Success",
        data: result,
    });
};
exports.responseErrorSender = (res, message, status, code) => {
    return res.status(code || 500).json({
        status: status || "Error",
        message: message,
    });
};
exports.ServerErrorHandler = (error, req, res, next) => {
    console.log('error',error);
    return res.status(500).json({
        status: "Error",
        message: error.message||error,
    });
};
