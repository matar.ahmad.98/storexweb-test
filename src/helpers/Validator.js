const {check} = require('express-validator');
exports.signInValidators = [
    check('password').exists().withMessage('is required').bail().isLength({min: 8}).withMessage('Must be at least 8 chars long').isLength({max: 20}).withMessage("Must be at most 20 chars long"),
    check('email').exists().withMessage('is required').bail().isEmail(),
];
exports.createValidators = [
    check('password').exists().withMessage('is required').bail().isLength({min: 8}).withMessage('Must be at least 8 chars long').isLength({max: 20}).withMessage("Must be at most 20 chars long"),
    check('email').exists().withMessage('is required').bail().isEmail(),
    check('name').exists().withMessage('is required').bail().isLength({min:4,max:15}),
];

exports.createOrUpdateCategoryValidators = [
    check('title').exists().withMessage('is required')
]
